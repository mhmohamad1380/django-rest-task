from rest_framework.permissions import BasePermission

class IsSuperuser(BasePermission):
    def has_object_permission(self, request, view, obj):
        return bool(
            request.user.is_superuser
        )

class IsCreator(BasePermission):
    def has_object_permission(self, request, view, obj):
        return bool(
            request.user == obj.user
        )