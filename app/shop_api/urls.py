from django.urls import path
from .views import ShopViewSet, ShopRetrieveDestroy

urlpatterns = [
    path("api/shop/list", ShopViewSet.as_view()),
    path("api/shop/<pk>", ShopRetrieveDestroy.as_view())
]