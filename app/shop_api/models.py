
from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver
import os
# Create your models here.


class Shop(models.Model):
    user = models.ForeignKey(
        User, null=True, on_delete=models.CASCADE, blank=True, editable=False)
    title = models.CharField(max_length=120, blank=False, null=True)
    price = models.PositiveIntegerField()
    

    def __str__(self):
        return f"{self.title} {self.user}"



class ShopPictures(models.Model):
    shop = models.ForeignKey(Shop,on_delete=models.CASCADE)
    picture = models.ImageField(upload_to="shop")

    def __str__(self):
        return str(self.shop)

@receiver(models.signals.pre_delete, sender=ShopPictures)
def delete_all_image_files(sender, instance, **kwargs):
    shop_relation = instance.shop.id
    for image in sender.objects.all():
        if instance.picture:
            if os.path.isfile(image.picture.path):
                if image.shop.id == shop_relation:
                    os.remove(image.picture.path)