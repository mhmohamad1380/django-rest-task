from rest_framework import serializers
from .models import Shop, ShopPictures

class ShopPicturesSerializer(serializers.ModelSerializer):
    class Meta:
        model = ShopPictures
        fields = ["picture"]


class ShopSerializer(serializers.HyperlinkedModelSerializer):
    pictures = serializers.SerializerMethodField()
    class Meta:
        model = Shop
        fields = ["id","title","price","pictures"]
    
    def get_pictures(self,obj):
        selected_pictures = ShopPictures.objects.filter(
            shop=obj).distinct()
        return ShopPicturesSerializer(selected_pictures, many=True).data