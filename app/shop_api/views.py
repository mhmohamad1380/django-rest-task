from django.http import HttpResponse
from django.shortcuts import render
from rest_framework.generics import ListAPIView,RetrieveDestroyAPIView
from .permissions import IsCreator, IsSuperuser
from shop_api.models import Shop
from rest_framework.permissions import AllowAny
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.authentication import BaseAuthentication
from shop_api.serializers import ShopSerializer
# Create your views here.


def home(request):
    return HttpResponse("Home Page...")


class ShopViewSet(ListAPIView):
    queryset = Shop.objects.all()
    permission_classes = [AllowAny]
    search_fields = ['title']
    filter_backends = [SearchFilter, OrderingFilter]
    serializer_class = ShopSerializer

class ShopRetrieveDestroy(RetrieveDestroyAPIView):
    queryset = Shop.objects.all()
    serializer_class = ShopSerializer
    permission_classes = [IsSuperuser]


    

