from django.contrib import admin

from shop_api.models import Shop, ShopPictures
from django.utils.html import format_html

# Register your models here.



class ShopPicturesTabular(admin.TabularInline):
    model = ShopPictures
    readonly_fields = ["thumbnail", "imgsize"]

    def imgsize(self, instance):
        if instance.image.name != "":
            return f"{int(instance.image.size / 1024)} KB"
        return ""

    def thumbnail(self, instance):
        if instance.image.name != "":
            return format_html(f"<img src='{instance.image.url}' class='thumbnail'/>")
        return ''

@admin.register(Shop)
class ShopAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        obj.user = request.user
        super().save_model(request, obj, form, change)

    inlines = [ShopPicturesTabular]

    class Media:
        css = {
            "all": ["styles.css"]
        }