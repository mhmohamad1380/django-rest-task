<div  align="center">
<h1  align="center">Dockerized Django rest framework</h1>
</div>

## Running

1. first, You should clone this Repository.<br/>
2. delete the txt file exists in db folder
3. at the third step, go to the Cloned Directory, then Open Terminal(CMD). <br/>
4. then, type ```docker-compose up --build ``` and Press Enter. (tip: make sure that Docker and Docker-Compose is Installed on Your Machine)
5. tip: username is ``` admin ``` and password is ``` admin ```

## Routes

1.Shop Items List: ```/api/shop/list```.<br/>
2.Shop Item Retrieve or Delete: ```/api/shop/<id>```

### Good Luck
